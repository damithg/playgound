using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadWriteJson
{
    public class DeprecatedLeadData
    {
        public Nullable<System.Guid> PersonId { get; set; }
        public string AgreementNumber { get; set; }
        public Nullable<decimal> LoanAmount { get; set; }
        public Nullable<byte> LoanTerm { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string PostCode { get; set; }
        public string HomeTel { get; set; }
        public string WorkTel { get; set; }
        public string MobileTel { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public Nullable<int> CampaignID { get; set; }
        public Nullable<decimal> BrokerFee { get; set; }
        public Nullable<bool> Seen { get; set; }
        public int Id { get; set; }
    }

    public class LeadData
    {
        public Guid? PersonId { get; set; }
        public string AgreementNumber { get; set; }
        public decimal? LoanAmount { get; set; }
        public byte? LoanTerm { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string PostCode { get; set; }
        public string HomeTel { get; set; }
        public string WorkTel { get; set; }
        public string MobileTel { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public Nullable<int> CampaignID { get; set; }
        public Nullable<decimal> BrokerFee { get; set; }
        public Nullable<bool> Seen { get; set; }
        public int Id { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
    }
}
