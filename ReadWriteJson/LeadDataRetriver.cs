﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ReadWriteJson
{
    public class LeadDataRetriver
    {
        private static int count = 0;
        private static Repository repo = new Repository();

        public void GetLeadData()
        {
            var leads = repo.GetLaedData().ToList();

            leads.ForEach(CreateJsonFile);
        }

        //TODO : This is only for testing to see what data is been coming from the sproc
        private static void CreateJsonFile(DeprecatedLeadData lead)
        {
            count++;
            var fileName = @"C:\Leads\" + lead.AgreementNumber + "_" + lead.PersonId + ".json";

            using (StreamWriter sw = File.CreateText(fileName))
            {
                var serializer = new JsonSerializer();
                serializer.Serialize(sw, lead);
            }

            //repo.UpdateLead(lead);

            System.Console.WriteLine(count);
        }

        private static List<DeprecatedLeadData> ReadJsonFile()
        {
            const string folderPath = @"C:\Leads";

            //var leadData = Directory.EnumerateFiles(folderPath, "*.json").Select(JsonConvert.DeserializeObject<DEPRECATED_LeadData>).ToList();
            var leadData = new List<DeprecatedLeadData>();
            foreach (string file in Directory.EnumerateFiles(folderPath, "*.json"))
            {
                leadData.Add(JsonConvert.DeserializeObject<DeprecatedLeadData>(File.ReadAllText(file)));
            }

            return leadData.Take(3).ToList();
        }

        private static void WriteJsonContents(DeprecatedLeadData lead)
        {
            count++;

            var leadData = new LeadData
            {
                Address1 = lead.Address1 != null ? Regex.Replace(lead.Address1.ToLower(), @"\b[a-z]", m => m.Value.ToUpper()) : "",
                Address2 = lead.Address2 != null ? Regex.Replace(lead.Address2.ToLower(), @"\b[a-z]", m => m.Value.ToUpper()) : "",
                Address3 = lead.Address3 != null ? Regex.Replace(lead.Address3.ToLower(), @"\b[a-z]", m => m.Value.ToUpper()) : "",
                AgreementNumber = lead.AgreementNumber,
                BrokerFee = lead.BrokerFee,
                CampaignID = lead.CampaignID,
                DateCreated = DateTime.Today,
                DateOfBirth = lead.DateOfBirth,
                Email = lead.Email,
                FirstName = lead.FirstName != null ? Regex.Replace(lead.FirstName.ToLower(), @"\b[a-z]", m => m.Value.ToUpper()) : "",
                HomeTel = lead.HomeTel,
                LoanAmount = lead.LoanAmount,
                LastName = lead.LastName != null ? Regex.Replace(lead.LastName.ToLower(), @"\b[a-z]", m => m.Value.ToUpper()) : "",
                LoanTerm = lead.LoanTerm,
                MobileTel = lead.MobileTel,
                PersonId = lead.PersonId,
                PostCode = lead.PostCode,
                Seen = lead.Seen,
                Title = lead.Title != null ? Regex.Replace(lead.Title.ToLower(), @"\b[a-z]", m => m.Value.ToUpper()) : "",
                WorkTel = lead.WorkTel,
            };

            var result = repo.AddLead(leadData);

            System.Console.WriteLine(string.Format("Writing {0} {1}", count, result == 1 ? "Pass" : "Skipped"));

        }
    }
}
