using System;
using System.Collections.Generic;
using System.Linq;

namespace IOC.StructureMap
{
    public class Resolver
    {
        //public ICreditCard ResolveCreditCard()
        //{
        //    if(new Random().Next(2) == 1)
        //        return new Visa();

        //    return new MasterCard();
        //}

        private Dictionary<Type, Type> dependancyMap = new Dictionary<Type, Type>();

        public T Resolve<T>()
        {
            return (T)Resolve(typeof(T));
        }

        private object Resolve(Type typeToResolve)
        {
            Type resolvedType = null;

            try
            {
                resolvedType = dependancyMap[typeToResolve];
            }
            catch (Exception)
            {
                throw new Exception(string.Format("Could not resolve {0}", typeToResolve.FullName));
            }

            var firstConstructer = resolvedType.GetConstructors().First();
            var constructerParameters = firstConstructer.GetParameters();

            if (constructerParameters.Count() == 0) return Activator.CreateInstance(resolvedType);

            IList<object> parameters = new List<object>();

            foreach (var parameterToResolve in constructerParameters)
            {
                parameters.Add(Resolve(parameterToResolve.ParameterType));
            }

            return firstConstructer.Invoke(parameters.ToArray());
        }

        public void Register<TFrom, TTo>()
        {
            dependancyMap.Add(typeof(TFrom), typeof(TTo));
        }
    }
}