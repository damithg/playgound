﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace IOC.StructureMap
{
    public class Class1
    {
        Resolver resolver = new Resolver();
        //var shopper = new Shopper(resolver.ResolveCreditCard());
        //var shopper = new Shopper(credtCard);

        public void TestThis()
        {
            resolver.Register<Shopper, Shopper>();
            resolver.Register<ICreditCard, MasterCard>();

            var shopper = resolver.Resolve<Shopper>();
            shopper.Charge();
        }
    }

    public class Visa : ICreditCard
    {
        public string Charge()
        {
            return "Swiping Visa Card";
        }
    }

    public class MasterCard : ICreditCard
    {
        public string Charge()
        {
            return "Swiping the Master Card";
        }
    }

    public class Shopper
    {
        private readonly ICreditCard creditcard;

        public Shopper(ICreditCard creditCard)
        {
            this.creditcard = creditCard;
        }

        public void Charge()
        {
            var chargeMessage = creditcard.Charge();
            Console.WriteLine(chargeMessage);
        }
    }

    public interface ICreditCard
    {
        string Charge();
    }
}
