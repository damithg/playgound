﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    public class DataComparerTests
    {
        [Test]
        public void blahblah()
        {
            

        }

        public static void Invoke()
        {
            var date1 = new DateTime(2014, 6, 19, 9, 50, 34);
            //DateTime date2 = new DateTime(2014, 6, 20, 10, 0, 0);
            DateTime date2 = DateTime.Now;
            int result = DateTime.Compare(date2, date1);
            //int result = DateTime.Compare(date1, DateTime.Now);
            string relationship;

            if (result < 0)
                relationship = "is earlier than";
            else if (result == 0)
                relationship = "is the same time as";
            else
                relationship = "is later than";

            Console.WriteLine("{0} {1} {2}", date1, relationship, date2);
        }

        public static class YieldExample
        {
            public static void Invoke()
            {

            }
        }

        public static class ListUpdater
        {
            static IEnumerable<Customer> myList = new List<Customer>();

            public static void Invoke()
            {
                myList.ToList().ForEach(x => x.IsUnread = (x.UnreadCount > 0));//Original products will be modified

                var returnedList = myList.Select(c => { c.IsUnread = (c.UnreadCount > 0); return c; }).ToList(); //Using just the framework 
            }
        }

        internal class Customer
        {
            public bool IsUnread { get; set; }
            public decimal UnreadCount { get; set; }
        }
    }
}
