using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    public class ConditionalOperators
    {
        [Test]
        public void Comparing_DateTime_Should()
        {
            // Arrange
            var today1 = DateTime.Now;
            var today2 = DateTime.Now;
            DateTime datetimeTomorrow = DateTime.Now.AddDays(1);

            // Act
            var t1EarlierThant2 = DateTime.Compare(today1, datetimeTomorrow) < 0; // t1 is earlier than t2
            var t1SamaAst2 = DateTime.Compare(today1, today1) == 0; // t1 is same as t2
            var t1GreaterThant2 = DateTime.Compare(datetimeTomorrow, today1) > 0; // t1 is later than t2

            // Assert
            Assert.IsTrue(t1EarlierThant2);
            Assert.IsTrue(t1SamaAst2);
            Assert.IsTrue(t1GreaterThant2);

        }

        [Test]
        public void Conditioanl_Result()
        {
            // Arrange
            var para1 = true;
            var para2 = false;

            // Act
            var result = para1 && para2;

            // Assert

            Assert.IsTrue(result); //false
        }

        [Test]
        public void Conditioan_Bit_Wise_Or_Result()
        {
            // Arrange
            var para1 = true;
            var para2 = false;

            // Act
            var result1 = para1 || para2;

            var result2 = para2 || para1;

            // Assert

            Assert.IsTrue(result1); //True
        }

        [Test]
        public void Convert_Month_Year_ToDate()
        {
            // Arrange
            var para1 = new DateTime(2015, 12, 01);
            var para2 = false;

            // Act
            //var result1 = para1 || para2;
            var result1 = true;
            //var result2 = para2 || para1;

            // Assert

            Assert.IsTrue(result1); //True
        }


        [Test]
        public void Does_Linq_Select_Throw_Exception_When_The_List_Is_Empty()
        {
            var param1 = new List<Template> { new Template { TemplateType = TemplateType.Email }, new Template { TemplateType = TemplateType.Sms } };
            var param2 = new List<Template>();

            var result1 = param1.Select(x => x.TemplateType).ToList();
            var result2 = param2.Select(x => x.TemplateType).ToList();

            Assert.IsTrue(result1.Any());
            Assert.IsFalse(result2.Any());


        }
    }

    public class Template
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FollowupId { get; set; }
        public int? Day { get; set; }
        public Guid? CommsTemplateId { get; set; }
        public bool? HasOtherParties { get; set; }
        public int MessageType { get; set; }
        public int FollowupType { get; set; }
        public int StopReSendDayRange { get; set; }
        public string FlagsToCheck { get; set; }
        public string DataPointsToCheck { get; set; }
        public Guid? SplitTestExternalReferenceId { get; set; }
        public bool? CheckReference { get; set; }
        public TemplateType TemplateType { get; set; }
        public string SmsRoute { get; set; }
        public string EmailRoute { get; set; }
        public string EmailFrom { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public int DelayMinutes { get; set; }
        public bool? IsTopupType { get; set; }
    }

    public enum TemplateType
    {
        Unknown = 0,
        Email = 6,
        Sms = 7,
        Letter = 8
    }
}
